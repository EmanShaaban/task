const { create, list } = require('../core/bid')

const createBid = (req, res) => {
  const bid = create(req.body)
  res.status(201)
  return bid
}

const listBids = (req, res) => {
  const { limit, from } = req.parsed.paginator
  const petId = parseInt(req.query.pet_id)
  return list(petId, limit, from)
}

module.exports = {
  createBid,
  listBids
}
