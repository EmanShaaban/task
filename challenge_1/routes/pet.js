const AsyncRouter = require('macropress-router')

const router = new AsyncRouter()

const petController = require('../controllers/pet')
const { paginatorMiddleware } = require('../middlewares/paginator')
const {
  createBidValidator,
  listBidsValidator
} = require('../validators/pet')

router.post('/bid', createBidValidator, petController.createBid)
router.get('/bids', paginatorMiddleware, listBidsValidator, petController.listBids)

module.exports = router
