const AsyncRouter = require('macropress-router')

const router = new AsyncRouter()

router.use('/', require('./health'))
router.use('/pet', require('./pet'))

module.exports = router
