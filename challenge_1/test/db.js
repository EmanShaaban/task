const faker = require('faker')
const { Bid, Pet } = require('../models')

const cleanUpDatabase = async () => {
  await Bid.destroy({ where: {}, force: true })
  await Pet.destroy({ where: {}, force: true })
}

const bidGenerator = async (bidData = {}) => {
  const bidObject = {
    userId: faker.datatype.number({ min: 0, max: 9999 }),
    petId: faker.datatype.number({ min: 0, max: 9999 }),
    amount: faker.datatype.number({ min: 0, max: 9999 })
  }

  const bid = await Bid.create({ ...bidObject, ...bidData })
  return bid
}

const petGenerator = async (petData = {}) => {
  const petObject = {
    name: faker.name.findName()
  }

  const pet = await Pet.create({ ...petObject, ...petData })
  return pet
}

module.exports = {
  cleanUpDatabase,
  bidGenerator,
  petGenerator
}
