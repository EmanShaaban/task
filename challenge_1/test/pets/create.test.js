const faker = require('faker')
const { expect, request } = require('../utils')
const { cleanUpDatabase } = require('../db')

describe('Create Bid POST /pet/bid', async () => {
  beforeEach(cleanUpDatabase)

  // json object to be sent in body
  const bidObject = {
    user_id: faker.datatype.number({ min: 0, max: 9999 }),
    pet_id: faker.datatype.number({ min: 0, max: 9999 }),
    amount: faker.datatype.number({ min: 0, max: 9999 })
  }

  it('should create a bid successfully', async () => {
    const res = await request.post('/pet/bid')
      .send(bidObject)

    const bid = res.body
    expect(res).to.have.status(201)
    expect(bid.userId).to.equal(bidObject.user_id)
    expect(bid.petId).to.equal(bidObject.pet_id)
    expect(bid.amount).equal(bidObject.amount)
  })

  it('should not create bid if one of the required fields is missing', async () => {
    delete bidObject.user_id // remove required field

    const res = await request.post('/pet/bid')
      .send(bidObject)

    expect(res).to.have.status(400)
  })
})
