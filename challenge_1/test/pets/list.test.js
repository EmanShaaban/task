const { expect, request } = require('../utils')
const { cleanUpDatabase, bidGenerator, petGenerator } = require('../db')

describe('list Bids GET /pet/bids', async () => {
  beforeEach(cleanUpDatabase)

  it('should list all bids of a pet', async () => {
    const pet = await petGenerator()

    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })
    await bidGenerator()

    const res = await request.get('/pet/bids')
      .query({ pet_id: pet.id })

    expect(res).to.have.status(200)
    expect(res.body).to.length.of.length(5)
  })

  it('should return 400 if there is invalid query param', async () => {
    const pet = await petGenerator()

    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })

    const res = await request.get('/pet/bids')
      .query({ fake: 2 })

    expect(res).to.have.status(400)
  })

  it('should list bids with pagination successfully', async () => {
    const pet = await petGenerator()

    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })
    await bidGenerator({ petId: pet.id })

    const res = await request.get('/pet/bids')
      .query({ pet_id: pet.id, page: 2, limit: 2 })

    expect(res).to.have.status(200)
    expect(res.body).to.length.of.length(2)
  })
})
