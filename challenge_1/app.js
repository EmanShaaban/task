const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')

const router = require('./routes')
const httpLogger = require('./middlewares/http-logger')
const { errorHandler, celebrateErrorsMiddleware } = require('./middlewares/error')

const app = express()

app.set('port', process.env.PORT || 5000)

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

// Middlewares
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(httpLogger)
app.use(router)
app.use(celebrateErrorsMiddleware)
app.use(errorHandler)

module.exports = { app }
