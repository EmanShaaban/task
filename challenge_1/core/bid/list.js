const Boom = require('boom')
const logger = require('../../config/logger')
const { Bid } = require('../../models')

const list = async (petId, limit, from) => {
  try {
    const bids = await Bid.findAll({
      where: { petId: petId },
      limit: limit,
      offset: from
    })
    return bids
  } catch (error) {
    logger.error('Error listing bids ', error)
    throw Boom.boomify(error, { statusCode: error.statusCode })
  }
}

module.exports = {
  list
}
