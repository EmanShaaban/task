const Boom = require('boom')
const logger = require('../../config/logger')
const { Bid } = require('../../models')

const create = async (params) => {
  let bid = {
    userId: params.user_id,
    petId: params.pet_id,
    amount: params.amount
  }

  try {
    bid = await Bid.create(bid)
    return bid
  } catch (error) {
    logger.error('Error creating bid! ', error)
    throw Boom.boomify(error, { statusCode: error.statusCode })
  }
}

module.exports = {
  create
}
