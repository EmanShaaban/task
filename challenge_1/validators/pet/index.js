const { createBidValidator } = require('./create')
const { listBidsValidator } = require('./read')

module.exports = {
  createBidValidator,
  listBidsValidator
}
