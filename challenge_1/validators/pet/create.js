const { Joi, celebrate } = require('celebrate')

const createBid = {
  body: Joi.object().keys({
    user_id: Joi.number().integer().required(),
    pet_id: Joi.number().integer().required(),
    amount: Joi.number().integer().required()
  })
}

module.exports = {
  createBidValidator: celebrate(createBid)
}
