const { Joi, celebrate } = require('celebrate')

const listBids = {
  query: {
    pet_id: Joi.number().integer().required(),
    page: Joi.number().integer(),
    limit: Joi.number().integer()
  }
}

module.exports = {
  listBidsValidator: celebrate(listBids)
}
