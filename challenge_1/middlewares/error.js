const Boom = require('boom')
const { isCelebrateError } = require('celebrate')

/**
 * Handle celebrate validation errors.
 */
const celebrateErrorsMiddleware = (err, req, res, next) => {
  // If this isn't a Celebrate error, send it to the next error handler
  if (!isCelebrateError(err)) {
    return next(err)
  }

  const boomError = Boom.badRequest(err.details.values().next().value)
  return next(boomError)
}

// Error Handler middleware
const errorHandler = (error, req, res, next) => {
  if (!error) return next()
  const payload = {
    status: error.status,
    message: error.message,
    stack: error.stack
  }
  return res.status(error.output.statusCode).json(payload)
}

module.exports = {
  errorHandler,
  celebrateErrorsMiddleware
}
