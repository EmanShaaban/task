# Pet Store

## Start
``` bash
docker-compose up
```

## Test
> Integration testing
``` bash
$ docker exec -it api sh
$ set -a; source test.env; set +a
$ yarn run create-db 
$ yarn run migrate
$ yarn test
```

## App Status
> returns the status of the service
http://localhost:5000/health

## Documentation
> Swagger Documentation
http://localhost:5000/docs/


 **Create bid** _POST_ `localhost:5000/pet/bid`:
> Request
```
{
  "pet_id": 3813,
  "user_id": 1807,
  "amount": 2534
}
```
> Response
```
{ 
"id":209,
"userId":1807,
"petId":3813,
"amount":2534,
"updatedAt":"2021-05-08T06:40:17.010Z",
"createdAt":"2021-05-08T06:40:17.010Z"
}
```

List bids request _GET_ `localhost:5000/pet/bids?pet_id=34&page=2&limit=2`:
> Response
```
[
  {
    "id": 218,
    "userId": 8658,
    "petId": 34,
    "amount": 4342,
    "status": null,
    "createdAt": "2021-05-08T06:47:54.000Z",
    "updatedAt": "2021-05-08T06:47:54.000Z"
  },
  {
    "id": 219,
    "userId": 4396,
    "petId": 34,
    "amount": 7408,
    "status": null,
    "createdAt": "2021-05-08T06:47:54.000Z",
    "updatedAt": "2021-05-08T06:47:54.000Z"
  }
]
```