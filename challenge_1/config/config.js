module.exports = {
  development: {
    use_env_variable: 'DATABASE_URI'
  },
  test: {
    use_env_variable: 'TEST_DATABASE_URI'
  },
  production: {
    use_env_variable: 'DATABASE_URI'
  }
}
