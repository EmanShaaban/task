const expect = require('chai').expect
const calculate_GSP = require('./gsp')

describe('Calculate GSP', () => {
    it('should return no winners in case of no bids', () => {
        const bids = {
            'John Doe': 0,
            'John Smith': 0,
            'Sara Conor': 0,
            'Martin Fowler': 0
        }
        expect(calculate_GSP(bids)).to.equal('No Winners')
    })

    it('should calculate GSP for bids successfully', () => {
        const items = 3
        const bids = {
            'John Doe': 100,
            'John Smith': 500,
            'Sara Conor': 280,
            'Martin Fowler': 320
        }

        expect(calculate_GSP(bids, items)).to.eql({
            'John Smith': 320,
            'Martin Fowler': 280,
            'Sara Conor': 100,
            'John Doe': 'Lost'
        })
    })

    it('should calculate GSP for bids successfully if only 1 item available', () => {
        const items = 1
        const bids = {
            'John Doe': 100,
            'John Smith': 500,
            'Sara Conor': 280,
            'Martin Fowler': 320
        }

        expect(calculate_GSP(bids, items)).to.eql({
            'John Smith': 320,
            'Martin Fowler': 'Lost',
            'Sara Conor': 'Lost',
            'John Doe': 'Lost'
        })
    })

    it('should calculate GSP for bids successfully regardless the bids order', () => {
        const items = 3
        const bids = {
            'Sara Conor': 280,
            'John Doe': 100,
            'John Smith': 500,
            'Martin Fowler': 320
        }

        expect(calculate_GSP(bids, items)).to.eql({
            'John Smith': 320,
            'Martin Fowler': 280,
            'Sara Conor': 100,
            'John Doe': 'Lost'
        })
    })

    it('should calculate GSP for bids successfully if there is equal bids', () => {
        const items = 3
        const bids = {
            'Sara Conor': 300,
            'John Doe': 100,
            'John Smith': 100,
            'Martin Fowler': 500
        }

        expect(calculate_GSP(bids, items)).to.eql({
            'Martin Fowler': 300,
            'Sara Conor': 100,
            'John Doe': 'Lost',
            'John Smith': 'Lost'
        })
    })
})