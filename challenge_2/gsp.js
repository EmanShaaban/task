const calculate_GSP = (bids, items) => {

    let out = {}
    const sortedBids = Object.entries(bids).sort((a, b) => b[1] - a[1])
    for (let i = 0; i < sortedBids.length; i++) {
        let name = sortedBids[i][0]
        let bid = sortedBids[i][1]

        if (bid === 0) {
            out = "No Winners"
            return out
        }


        if (items === 0) {
            out[name] = 'Lost'

        } else {
            let next_name = sortedBids[i + 1][0]
            let next_bid = sortedBids[i + 1][1]
            if (bid === next_bid) {
                if (name > next_name) {
                    out[name] = next_bid
                    out[next_name] = 'Lost'
                    items--
                } else {
                    out[name] = 'Lost'
                    out[next_name] = next_bid
                    items--
                }
            }
            else {
                out[name] = next_bid
                items--
            }
        }
    }

    return out
}


module.exports = calculate_GSP